#!/bin/bash

#Step2:
wget http://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img
#step 3
sudo virt-customize -a xenial-server-cloudimg-amd64-disk1.img \
      --run-command 'apt-get update' \
      --run-command 'DEBIAN_FRONTEND=noninteractive' \
      --run-command 'apt-get install -y software-properties-common net-tools inetutils-ping iptables aptitude git wget python-dev python-pip python python-yaml curl iproute2 iperf3 iperf apache2-utils siege httperf' \
      --upload start.sh:/start.sh \
      --upload stop_ab.sh:/stop_ab.sh \
      --run-command 'chmod +x start.sh' \
      --run-command 'chmod +x stop_ab.sh' \
      --upload stop_iperf.sh:/stop_iperf.sh \
      --run-command 'chmod +x stop_iperf.sh' \
      --upload stop_simple_exit.sh:/stop_simple_exit.sh \
      --run-command 'chmod +x stop_simple_exit.sh' \
      --upload stop_siege.sh:/stop_siege.sh \
      --run-command 'chmod +x stop_siege.sh' \
      --upload stop_siege.sh:/stop_siege.sh \
      --upload process_iperf3_results.py:/process_iperf3_results.py \
      --upload process_ab_results.py:/process_ab_results.py \
      --upload log_intf_statistics.py:/log_intf_statistics.py \
      --run-command 'cd /' \
      --run-command 'echo "manage_etc_hosts: true" >> /etc/cloud/cloud.cfg' \
      --mkdir /tngbench_share

#step4
source /opt/stack/devstack/accrc/admin/admin
openstack image create --public --disk-format qcow2 --container-format bare --file xenial-server-cloudimg-amd64-disk1.img traffic-img