function done(summary, latency, requests)
    file = io.open('/tngbench_share/result_intermediate.json', 'w')
    io.output(file)
  
    io.write(string.format("{\n"))
  
    io.write(string.format("  \"summary\": {\n"))
    io.write(string.format("    \"duration_microseconds\": %d,\n",      summary.duration))
    io.write(string.format("    \"num_requests\":          %d,\n",      summary.requests))
    io.write(string.format("    \"total_bytes\":           %d,\n",      summary.bytes))
    io.write(string.format("    \"requests_per_sec\":      %.2f,\n",    summary.requests/(summary.duration)))
    io.write(string.format("    \"errors_connect\":      %.2f,\n",    summary.errors.connect))
    io.write(string.format("    \"errors_read\":      %.2f,\n",    summary.errors.read))
    io.write(string.format("    \"errors_write\":      %.2f,\n",    summary.errors.write))
    io.write(string.format("    \"errors_status\":      %.2f,\n",    summary.errors.status))
    io.write(string.format("    \"errors_timeout\":      %.2f,\n",    summary.errors.timeout))

    io.write(string.format("    \"requests_stdev\":      %.2f,\n",requests.stdev))
    io.write(string.format("    \"requests_avg\":      %.2f,\n",requests.mean))
    io.write(string.format("    \"requests_min\":      %.2f,\n",requests.min))
    io.write(string.format("    \"requests_max\":      %.2f,\n",requests.max))
    io.write(string.format("    \"bytes_per_sec\":         \"%.2f\"\n", summary.bytes/summary.duration))
    io.write(string.format("  },\n"))
  
    io.write(string.format("  \"latency\": {\n"))
    io.write(string.format("    \"lat_min_microseconds\":           %.2f,\n", latency.min))
    io.write(string.format("    \"lat_max_microseconds\":           %.2f,\n", latency.max))
    io.write(string.format("    \"lat_mean_microseconds\":          %.2f,\n", latency.mean))
    io.write(string.format("    \"lat_stdev_microseconds\":         %.2f,\n", latency.stdev))
    io.write(string.format("    \"lat_percentile_50_microseconds\": %.2f,\n", latency:percentile(50.0)))
    io.write(string.format("    \"lat_percentile_75_microseconds\": %.2f,\n", latency:percentile(75.0)))
    io.write(string.format("    \"lat_percentile_90_microseconds\": %.2f,\n", latency:percentile(90)))
    io.write(string.format("    \"lat_percentile_99_microseconds\": %.2f,\n",  latency:percentile(99.0)))
    io.write(string.format("    \"lat_percentile_100_microseconds\": %.2f\n", latency:percentile(99.999)))
    io.write(string.format("  }\n"))
  
  
    io.write(string.format("}\n"))
  end