#!/bin/bash

wget http://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img

virt-customize -a xenial-server-cloudimg-amd64-disk1.img \
      --run-command 'apt-get update' \
      --run-command 'DEBIAN_FRONTEND=noninteractive' \
      --run-command 'apt-get install -y \
                      software-properties-common \
                      net-tools \
                      iproute2 \
                      inetutils-ping \
                      iptables \
                      aptitude \
                      wget \
                      curl \
                      iperf3 \
                  		iperf \
                      apache2-utils \
                      python \
                      python-yaml \
                      python3 \
                      python3-pip wrk' \
      --run-command 'cd /' \
      --upload start.sh:/start.sh \
      --upload stop.sh:/stop.sh \
      --upload stop_wrk.sh:/stop_wrk.sh \
      --upload wrk_json.lua:/wrk_json.lua \
      --upload process_wrk_results.py:/process_wrk_results.py \
      --upload log_intf_statistics.py:/log_intf_statistics.py \
      --upload process_ab_results.py:/process_ab_results.py \
      --run-command 'chmod +x start.sh' \
      --run-command 'chmod +x stop.sh' \
      --run-command 'chmod +x stop_wrk.sh' \
      --run-command 'echo "manage_etc_hosts: true" >> /etc/cloud/cloud.cfg' \
      --mkdir /tngbench_share

source /opt/stack/devstack/accrc/admin/admin
openstack image create --public --disk-format qcow2 --container-format bare --file xenial-server-cloudimg-amd64-disk1.img traffic-img
