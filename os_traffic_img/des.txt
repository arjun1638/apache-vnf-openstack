sudo virt-customize -v -x -a traffic_gen.img \
      --run-command 'apt-get update' \
      --run-command 'apt-get install -y   software-properties-common aptitude git wget python-dev python-pip python python-yaml net-tools gnuplot curl iproute inetutils-ping iperf3 iperf apache2-utils siege httperf' \
      --upload start.sh:/start.sh \
      --upload stop_ab.sh:/stop_ab.sh \
      --run-command 'chmod +x start.sh' \
      --run-command 'chmod +x stop_ab.sh' \
      --upload stop_iperf.sh:/stop_iperf.sh \
      --run-command 'chmod +x stop_iperf.sh' \
      --upload stop_simple_exit.sh:/stop_simple_exit.sh \
      --run-command 'chmod +x stop_simple_exit.sh' \
      --upload stop_siege.sh:/stop_siege.sh \
      --run-command 'chmod +x stop_siege.sh' \
      --upload stop_siege.sh:/stop_siege.sh \
      --upload process_iperf3_results.py:/process_iperf3_results.py \
      --upload process_ab_results.py:/process_ab_results.py \
      --upload log_intf_statistics.py:/log_intf_statistics.py \
      --run-command 'cd /' \
      --mkdir /tngbench_share