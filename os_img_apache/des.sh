#!/bin/bash
  
#Step2:
wget http://cloud-images.ubuntu.com/xenial/current/xenial-server-cloudimg-amd64-disk1.img

#Step3:
virt-customize -a xenial-server-cloudimg-amd64-disk1.img \
      --run-command 'apt-get update ' \
      --run-command 'DEBIAN_FRONTEND=noninteractive' \
      --run-command 'apt-get install -y software-properties-common iptables libguestfs-tools apache2 php7.0 php7.0-mysql libapache2-mod-php7.0 curl lynx-common lynx net-tools  iproute2 inetutils-ping curl python python-yaml' \
      --run-command 'a2enmod php7.0' \
      --run-command 'a2enmod rewrite' \
      --run-command 'sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.0/apache2/php.ini' \
      --run-command 'sed -i "s/error_reporting = .*$/error_reporting = E_ERROR | E_WARNING | E_PARSE/" /etc/php/7.0/apache2/php.ini' \
      --run-command 'echo "ServerName localhost" >> /etc/apache2/apache2.conf' \
      --run-command 'sed -i "/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/" /etc/apache2/apache2.conf' \
      --upload env.sh:/env.sh \
      --run-command 'chmod +x env.sh' \
      --run-command 'mkdir /var/www/site' \
      --upload index.php:/var/www/site/index.php \
      --upload apache-config.conf:/etc/apache2/sites-enabled/000-default.conf \
      --run-command 'cd /' \
      --upload log_intf_statistics.py:/log_intf_statistics.py \
      --upload start.sh:/start.sh \
      --upload stop.sh:/stop.sh \
      --run-command 'chmod +x start.sh' \
      --run-command 'chmod +x stop.sh' \
      --run-command 'echo "manage_etc_hosts: true" >> /etc/cloud/cloud.cfg' \
      --mkdir /tngbench_share


source /opt/stack/devstack/accrc/admin/admin
openstack image create --public --disk-format qcow2 --container-format bare --file xenial-server-cloudimg-amd64-disk1.img apache-img