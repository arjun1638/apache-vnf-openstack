#! /bin/bash
sleep 5
sudo chmod -R 777 /tngbench_share/
python ./log_intf_statistics.py /tngbench_share/result.yml
sleep 1
python ./process_wrk_results.py /tngbench_share/cmd_start.log /tngbench_share/result_intermediate.json /tngbench_share/result.yml
date > /tngbench_share/stop_time.txt